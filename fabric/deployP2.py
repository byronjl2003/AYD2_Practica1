from fabric import task,Connection
@task
def deployP2(ctx):

        connect_kwargs = {"key_filename":['.ssh/MiPrimeraClave.pem']}
        print("Voy a deplegar a 52.15.124.150!")
        c = Connection(host='52.15.124.150',user="ubuntu",connect_kwargs=connect_kwargs)
        result = c.run("mkdir abds")
        result = c.put('./docker-composeP2.yml')
        result = c.run('docker-compose -f docker-composeP2.yml  up --build -d')